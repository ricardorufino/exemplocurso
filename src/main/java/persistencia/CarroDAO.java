/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistencia;

import entidade.Carro;
import java.io.Serializable;
import java.util.List;
import javax.inject.Inject;
import javax.persistence.EntityManager;

/**
 *
 * @author ricardo
 */
@Transacional
public class CarroDAO implements Serializable{

    @Inject
    private EntityManager em;

    public void salvar(Carro carro) {
        em.merge(carro);
    }

    public void excluir(Carro carro) {
        em.remove(em.merge(carro));
    }

    public List<Carro> listar() {
        return em.createQuery("FROM Carro").getResultList();
    }

}
