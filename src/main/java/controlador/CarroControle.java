/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import entidade.Carro;
import java.io.Serializable;
import java.util.List;
import persistencia.CarroDAO;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author ricardo
 */
@Named
@RequestScoped
public class CarroControle implements Serializable {

    private Carro carro = new Carro();
    @Inject
    private CarroDAO carroDAO;

    public String salvar() {
        carroDAO.salvar(carro);
        return "index?faces-redirect=true";
    }
    
    public void excluir(Carro c){
        carroDAO.excluir(c);
    }

    public Carro getCarro() {
        return carro;
    }

    public void setCarro(Carro carro) {
        this.carro = carro;
    }

    public List<Carro> getCarrosCadastrados() {
        return carroDAO.listar();
    }

}
